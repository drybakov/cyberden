<?php

error_reporting( E_ALL );
ini_set( 'display_errors', 1);

$room=isset($_POST['stream_id']) ? $_POST['stream_id'] : '';

$currentTime=isset($_POST['ct']) ? $_POST['ct'] : '';
$lastTime=isset($_POST['lt']) ? $_POST['lt'] : '';
$disconnect=""; //anything else than "" will disconnect with that message

function sanV(&$var, $file=1, $html=1, $mysql=1) //sanitize variable depending on use
{
	if (!$var) return;

	if (get_magic_quotes_gpc()) $var = stripslashes($var);
	
	if ($file)
	{
		$var=preg_replace("/\.{2,}/","",$var); //allow only 1 consecutive dot
		$var=preg_replace("/[^0-9a-zA-Z\.\-\s_]/","",$var); //do not allow special characters
	}
	
	if ($html&&!$file)
	{
		$var=strip_tags($var);
		$forbidden=array("<", ">");
		foreach ($forbidden as $search)  $var=str_replace($search,"",$var);
	}

	if ($mysql&&!$file) 
	{
		$forbidden=array("'", "\"", "ґ", "`", "\\", "%");
		foreach ($forbidden as $search)  $var=str_replace($search,"",$var);
		$var=mysql_real_escape_string($var);
	}
}

/*
    varsToSend.hash=hash;
    varsToSend.stream_id=stream_id;
    varsToSend.timestamp_now=timestamp_now;
*/

//code to keep track of online broadcasters and total channel time:
//all usage time ads up (broadcaster + viewers): 10 min broadcast to 2 viewers = 30 min total usage
//do not allow uploads to other folders

  require_once('rediska/Rediska.php');
  $rediska = new Rediska( array( 'namespace' => 'broadcasts_vshare' ) );
  
  sanV($room);
  
  if ($room)
  {
    if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
      $ip=$_SERVER['HTTP_X_REAL_IP'];
    }
    elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    $rediska->setAndExpire( 'address_'.$ip, true, 30 );
        
    $sess_name = 'cast_'.$room;
    $time_start = time();
    if( $rediska->exists($sess_name) ) {
      $time_start = $rediska->get($sess_name);
    }
    $rediska->setAndExpire( 'cast_'.$room, $time_start, 30 );
    echo $room;
  }
//    $disconnect = "No valid room or session!";
?>
