<?php
header('Content-Type: text/html; charset=utf-8');

// change the following paths if necessary
$yii=dirname(__FILE__).'/../../library/framework/yii.php';
$config=dirname(__FILE__).'/../protected/config/main.php';
$definitions=dirname(__FILE__).'/../protected/config/definitions.php';

include_once($definitions);
require_once($yii);
Yii::createWebApplication($config)->run();
