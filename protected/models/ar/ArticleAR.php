<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $title
 * @property string $short
 * @property string $full
 * @property string $created_at
 * @property string $type
 * @property integer $publish
 * @property string $keywords
 * @property string $description
 * @property integer $export
 */
class ArticleAR extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArticleAR the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, keywords, description', 'required'),
			array('publish, export', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>256),
			array('type', 'length', 'max'=>16),
			array('keywords, description', 'length', 'max'=>1024),
			array('short, full, created_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, short, full, created_at, type, publish, keywords, description, export', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'short' => 'Short',
			'full' => 'Full',
			'created_at' => 'Created At',
			'type' => 'Type',
			'publish' => 'Publish',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'export' => 'Export',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('full',$this->full,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('publish',$this->publish);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('export',$this->export);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}