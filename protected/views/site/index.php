<?php
/* @var $this SiteController */

$this->pageTitle='Главная страница';

Yii::app()->clientScript->registerScriptFile(
        Yii::app()->baseUrl . '/js/main.js',
	CClientScript::POS_END
	);

?>
<?php if( $main ): ?>
<p id="preambula">
</p>
<div id="main_article">
<h2>Разработка: <a href="/design/<?php echo $main->id; ?>"><?php echo $main->title; ?></a></h2>
<div class="date"><?php echo date( 'd.m.Y H:i', strtotime($main->created_at) ); ?></div>
<div><?php echo $main->short; ?></div>
<div class="article_bottom">
<div class="more"><a href="/design/<?php echo $main->id; ?>">читать далее &rarr;</a></div>
<a href="http://cyberden.ru/design/id/<?php echo $main->id; ?>#disqus_thread" data-disqus-identifier="design/<?php echo $main->id; ?>" class="comments">Комментарии</a>
</div>
<div style="clear: both"></div>
</div>
<?php endif; ?>

<div id='scroller'>
  <div class='scroll_inner'>
  <div class='element' id='elem1'>
    <div class='left'>
    <h2></h2>
    <p>
    </p>
    </div>
    <a href="#" onclick='return false;' id='link1' class='right'><img src='/images/right.jpg' alt='далее' title='далее'/></a>
    <div style='clear: both'></div>
  </div>

  <div class='element' id='elem2'>
    <div class='left'>
    <h2></h2>
    <p>
    </p>
    </div>
    <a href="#" onclick='return false;' id='link2' class='right'><img src='/images/right.jpg' alt='далее' title='далее'/></a>
    <div style='clear: both'></div>
  </div>
  
  <div class='element' id='elem3'>
    <div class='left'>
    <h2></h2>
    <p>
    </p>
    </div>
    <a href="#" onclick='return false;' id='link3' class='right'><img src='/images/right.jpg' alt='назад' title='назад' /></a>
    <div style='clear: both'></div>
  </div>

  <div style='clear: both'></div>
  </div>
</div>

<div id="bottom_left">
<h3>Заметки:</h3>
<?php if( $list ): ?>
  <ul id="main_list">
<?php foreach($list as $item): ?>
  <li>
    <h3><a href="/<?php echo $item->type .'/'.$item->id ?>"><?php echo $item->title ?></a></h3>
    <div class="date"><?php echo date( 'd.m.Y', strtotime($item->created_at) ); ?></div>
 </li>
<?php endforeach; ?>
  </ul>
<?php endif; ?>
</div>
<div id="bottom_right">
<h3>Подписка:</h3>
<a id="feed" class="allowed" href="/rss">rss лента</a>
<a id="vk_link" href="#">Вконтакте</a>
<a id="twitter_link">Twitter</a>
<a id="fb_link" href="#">Facebook</a>
</div>
<div style="clear: both"></div>
<script type="text/javascript">
    var disqus_shortname = 'cyberden';
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>

