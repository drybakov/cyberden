<?php $this->pageTitle='Опыт и навыки'; ?>
<div class="left">
<h3 id="exp" class="info">Опыт</h3>
  <ul class="exp">
<li><b>Научная и преподавательская работа</b>
  <p class="date">09.2004 - 08.2006</p>
  Продвижение отечественной научной мысли к сияющим вершинам мирового господства.<br>
</li>
  </ul>
</div>
<div class="right">
  <h3 id="education" class="info">Образование</h3>
  <p>
  В 2004 г. окончил Механико-математический факультет МГУ им. М.В. Ломоносова.<br>
  <b>Квалификация:</b> Механик.<br>
  <b>Специальность:</b> Механика. Прикладная математика.<br>
  <b>Тема дипломной работы</b>: &laquo;Математические модели и алгоритмы управления мобильным трехколесным роботом с
  ведущим передним колесом&raquo;.
  </p>
  <h3 id="skills" class="info">Навыки</h3>
  <p>
    <b>Технические:</b><br><br>
    <img src="/images/zce-php5-3-logo.gif" style="float: left; margin-right: 10px; margin-bottom: 30px"/>
    PHP 5.3 Zend Certified Engineer<br>
    C/C++, SQL, JS<br>
    MySQL, Oracle<br>
  </p>
  <p>
  <b>Организационные:</b><br><br>
  </p>
  <h3 id="interest" class="info">Интересы</h3>
</div>
<div style="clear: both"></div>