<?php
$this->pageTitle="Разработка";
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'enablePagination' => true,
        'summaryText' => '',
        'pager' => Array(
                        'header' => 'Перейти на страницу: ',
                        'prevPageLabel' => 'Назад',
                        'nextPageLabel' => 'Далее',
                        'cssFile'=>Yii::app()->theme->baseUrl."/css/pager.css",
            ),
));
?>
<script type="text/javascript">
    var disqus_shortname = 'cyberden';
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>

