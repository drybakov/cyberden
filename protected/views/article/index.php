<?php
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'enablePagination' => true,
        'summaryText' => '',
        'pager' => Array(
                        'header' => 'Перейти на страницу: ',
                        'prevPageLabel' => 'Назад',
                        'nextPageLabel' => 'Далее',
            ),
));
?>
