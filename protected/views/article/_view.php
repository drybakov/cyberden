<?php
$link = $data->type.'/'.$data->id;
?>

<h2><?php echo CHtml::link( CHtml::encode($data->title), array( $link ) ); ?></h2>
<div class="date"><?php echo date( 'd.m.Y', strtotime($data->created_at) ); ?></div>
<?php echo $data->short; ?>
<div class="article_bottom">
<div class="more">
<a href="<?php echo $link; ?>">читать далее →</a>
</div>
<a href="<?php echo $link; ?>#disqus_thread" data-disqus-identifier="<?php echo $link; ?>" class="comments">0 комментариев</a>
</div>
<div style="clear: both;"/>