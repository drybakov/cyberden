<?php $this->pageTitle=$model->title; ?>

<h1><?php echo $model->title; ?></h1>
<div class="date"><?php echo date( 'd.m.Y', strtotime($model->created_at) ); ?></div>
<div>
<?php echo $model->short.$model->full; ?>
<div id="disqus_thread"></div>

<script type="text/javascript">
  var disqus_shortname = 'cyberden';
  var disqus_identifier = '<?php echo $model->type.'/'.$model->id; ?>';
  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function() {
   var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
   dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
</script>
<div style='clear: both'></div>
</div>
