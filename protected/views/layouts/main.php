<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="description" content="{nocache}{$description}{/nocache}" />
<meta name="keywords" content="{nocache}{$keywords}{/nocache}" />
  <script src="/js/swfobject.js"></script>
  <script src="/js/mootools12-core.js"></script>
  <script src="/js/mootools12-more.js"></script>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
</head>
<body>
<div id='wrapper'>
  <div id='head'>
  <div id="head_inner" class='wrapper'>  
    <div id="logo">
        <a href='/'>
      <h4>Заголовок старого сайта</h4>
      </a>
    </div>
    <div style="float: right">
    </div>
  </div>
  </div>
    
    <div id='container' class='wrapper'>
        <img src="/images/coding.png" />
    </div>
</div>
  <div id='footer'>
  <div class="left">
    <p id="created">Сайт создан в 2009 году</p>
    <p>Последнее обновление: <?php $this->widget('LastUpdateWidget' ); ?></p>
  </div>
  <div id="aux_menu">
  </div>
  </div>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>
<script type="text/javascript">
/*
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6300293-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  */
</script>
</body>
</html>