<?php

class DefaultController extends Controller
{
  public $layout='/layouts/column1';
  
  public function actions() {
    return array(
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xFFFFFF,
        ),
        );
  }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function accessRules()
    {
	return array(
	    array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('login', 'logout', 'captcha'),
                'users'=>array('guest', 'admin'),
	    ),
	    array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => function() { Yii::app()->controller->redirect(array ('/admin/default/login')); }
	    ),
	);
    }

    public function actionIndex()
    {
	$this->render('index');
    }
	
    public function actionLogin()
    {
	$model=new LoginForm;
	if (isset($_SERVER['HTTP_REFERER']))
	{
	  Yii::app()->user->setReturnUrl($_SERVER['HTTP_REFERER']);
	}
	
	if(isset($_POST['LoginForm']))
	{
		$model->attributes=$_POST['LoginForm'];
		if($model->validate() && $model->login())
			$this->redirect('/admin');
	}
	$this->render('login',array('model'=>$model));
    }

    public function actionLogout()
    {
          Yii::app()->user->logout();
          $this->redirect(Yii::app()->homeUrl);
    }
}