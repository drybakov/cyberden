<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_EXPIRED = 100;
    public $username;
    public $password;
    private $_id;
    
    public function __construct( $username, $password ) {
        $this->password = $password;
        $this->username = $username;
    }

    public function authenticate() {
        $users=array(
            'admin'=>'Hc7QwwKj',
        );
        if( !isset( $users[$this->username] ) )
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if( $users[$this->username] !== $this->password )
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else
            $this->errorCode=self::ERROR_NONE;
        
        if( !$this->errorCode )
            $this->_id = 'admin';
        
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
}