<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */
Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'article-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'model' => $model,
    'attribute' => 'short',
    'name' => 'short',
    'options' => array(
        'lang' => 'ru',
    'imageUpload' => Yii::app()->createAbsoluteUrl('site/upload'),
    'imageGetJson' => Yii::app()->createAbsoluteUrl('site/listimages')
    ),
)); ?>
		<?php echo $form->error($model,'short'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'full'); ?>
	    <?php
	    $this->widget('ImperaviRedactorWidget', array(
    'model' => $model,
    'attribute' => 'full',
    'name' => 'full',
    'options' => array(
        'lang' => 'ru',
    'imageUpload' => Yii::app()->createAbsoluteUrl('site/upload'),
    'imageGetJson' => Yii::app()->createAbsoluteUrl('site/listimages')
    ),
)); ?>

		<?php echo $form->error($model,'full'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList( $model, 'type', Article::getTypes() ); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywords'); ?>
		<?php echo $form->textField($model,'keywords',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'keywords'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'publish'); ?>
		<?php echo $form->checkBox($model,'publish'); ?>
		<?php echo $form->error($model,'publish'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'export'); ?>
		<?php echo $form->checkBox($model,'export'); ?>
		<?php echo $form->error($model,'export'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->