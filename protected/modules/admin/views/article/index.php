<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->menu=array(
	array('label'=>'Create Article', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('article-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Articles</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$dataProvider = $model->search();
$dataProvider->pagination = array('pageSize'=>20);
$dataProvider->sort = array('attributes'=>array('created_at'));
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'article-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
    	array(
	    'name' =>'title',
	    'type'=>'raw',
	    'value'=>'CHtml::link($data->title, "/admin/article/update?id=".$data->id )',
	),
	    array(
		'name' => 'created_at',
		'value' => 'date("d/m/Y H:i",strtotime($data->created_at))',
	        'filter' => false,
	    ),
	    array(
		'name' => 'type',
		'value' => '$data->type',
		'filter' => array_merge( array( '' => 'All' ), Article::getTypes() ),
	    ),
		array(
		    'class'=>'CButtonColumn',
		    'template'=>'{delete}',
		),
    ),
)); ?>
