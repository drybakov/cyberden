<?php

class Article extends ArticleAR
{
    const TYPE_BLOG = 'blog';
    const TYPE_DESIGN = 'design';
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeValidate(  ) {
        if( $this->isNewRecord ) {
          $this->created_at = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }
    
    public static function getTypes() {
	return array(
	    self::TYPE_BLOG => 'Blog',
	    self::TYPE_DESIGN => 'Design'
	);
    }
/*    public static function getTypes() {
	return array(
	    array( 'type' => self::TYPE_BLOG, 'title' => 'Blog' ),
	    array( 'type' => self::TYPE_DESIGN, 'title' => 'Design' )
	);
    }*/

    
}
?>
