<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class LastUpdateWidget extends CWidget {

    public $style = 'default';
    
    public function run() {
	$last = Article::model()->find(
		array(
		    'condition' => 'publish = 1',
		    'order' => 'created_at DESC'
		)
	);
	//echo date( 'd.m.Y', strtotime($item->created_at) )
        $created_at = date('d.m.Y');
        if ($last) {
            $created_at = $last->created_at;
        }
	echo date( 'd.m.Y H:i', strtotime( $created_at ));
    }
}
?>
