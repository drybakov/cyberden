<?php

class ArticleController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','blog','design','view'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Article');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

		/**
	 * Lists all models.
	 */
	public function actionBlog()
	{
		$dataProvider=new CActiveDataProvider('Article',array(
      'criteria' => array(
      'condition' => 'publish = 1 AND "type"=\'blog\'',
      'limit' => 3,
      'order' => 'created_at DESC',
      ),
      'pagination'=>array(
        'pageSize'=>5,
      ),
    )
    );
    
		$this->render('blog',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
		/**
	 * Lists all models.
	 */
	public function actionDesign()
	{
		$dataProvider=new CActiveDataProvider('Article',array(
      'criteria' => array(
      'condition' => 'publish = 1 AND "type"=\'design\'',
      'limit' => 3,
      'order' => 'created_at DESC',
      ),
      'pagination'=>array(
        'pageSize'=>5,
      ),
    ));
		$this->render('design',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Article::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='article-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
