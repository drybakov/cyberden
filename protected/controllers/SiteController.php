<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
    $main = Article::model()->findByAttributes( array( 'type' => 'design', 'publish' => 1 ), array( 'order' => 'created_at DESC' ) );
    $last = [];
        if ( $main ) {
            $last = Article::model()->findAll( array( 'condition' => 'id != '.$main->id.' AND publish = 1', 'order' => 'created_at DESC', 'limit' => 3 ));
        }
		$this->render('index', array(
    'main' => $main,
    'list' => $last
    ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionListimages()
	{
  	$images = array();

    $handler = opendir(Yii::app()->basePath.'/../public/files');

    while ($file = readdir($handler))
    {
        if ($file != "." && $file != "..")
            $images[] = $file;
    }
    closedir($handler);

    $jsonArray=array();

    foreach($images as $image)
        $jsonArray[]=array(
            'thumb'=>Yii::app()->baseUrl.'/files/'.$image,
            'image'=>Yii::app()->baseUrl.'/files/'.$image
        );

    header('Content-type: application/json');
    echo CJSON::encode($jsonArray);
	}
	
	public function actionUpload()
	{
    $file=CUploadedFile::getInstanceByName('file');
    if($file->saveAs(Yii::app()->basePath.'/../public/files/'.$file->name))
    {
        echo CHtml::image(Yii::app()->baseUrl.'/files/'.$file->name);
        Yii::app()->end();
    }

    throw new CHttpException(403,'The server is crying in pain as you try to upload bad stuff');
	}
	
	public function actionRss() {
	    Yii::import('ext.feed.*');
	    $feed = new EFeed();

	    $feed->title= 'Testing RSS 2.0 EFeed class';
	    $feed->description = 'This is test of creating a RSS 2.0 Feed';

	    $feed->addChannelTag('pubDate', date(DATE_RSS, time()));
	    $feed->addChannelTag('link', Yii::app()->getBaseUrl(true).'/rss' );

	    // TODO: use a query/foreach
	    $items = Article::model()->findAll( array( 'condition' => 'publish = 1/* AND export = 1*/', 'order' => 'created_at DESC', 'limit' => 10 ));
	    foreach( $items as $article ) {
		$item = $feed->createNewItem();
		$item->title = $article->title;
		$item->link = Yii::app()->getBaseUrl(true).'/'.$article->type.'/'.$article->id;
		$item->date = $article->created_at;
		$item->description = strip_tags( $article->short );
		// this is just a test!!
		$item->addTag('guid', Yii::app()->getBaseUrl(true).'/'.$article->type.'/'.$article->id,array('isPermaLink'=>'true'));
		$feed->addItem($item);
	    }
	    if( $items ) {
		$feed->generateFeed();
	    }
	    Yii::app()->end();
	}
}
