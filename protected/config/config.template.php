<?php
return array(
	'name'	  =>'@name@',
  'theme'		=> '@theme@',
	'homeUrl'	=>'@homeUrl@',

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'pgsql:port=@dbPort@;host=@dbHost@;dbname=@dbName@',
			'emulatePrepare' => true,
			'username' => '@dbUser@',
			'password' => '@dbPass@',
			'enableProfiling'=> @enableProfiling@,
			'charset' => 'utf8',
		),
  ),
);
